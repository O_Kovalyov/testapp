package com.gflexy.testapp

import android.app.Application
import timber.log.Timber

class App: Application() {

    companion object{
        const val API_KEY = "0599facbddbd33b1521b3f7feab8bef5"
    }

    override fun onCreate() {
        super.onCreate()
        initTimber()
    }

    private fun initTimber() {
        Timber.plant(
            object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String? {
                    return String.format(
                        "@@%s.%s:%d:T[%s]",
                        super.createStackElementTag(element),
                        element.methodName, element.lineNumber, Thread.currentThread().name
                    )
                }
            }
        )
    }
}