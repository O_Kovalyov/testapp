package com.gflexy.testapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gflexy.testapp.adapters.ViewPagerTabContentAdapter
import com.gflexy.testapp.decoratos.TabContentItemsDecorator
import com.gflexy.testapp.model.FragmentContent
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main_view_pager_tab.*

class InnerFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_main_view_pager_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        rv_view_pagers_tab_content.apply {
            arguments?.let {
                val photos: FragmentContent =
                    Gson().fromJson(it.getString("content"), FragmentContent::class.java)
                layoutManager = LinearLayoutManager(context)
                adapter = ViewPagerTabContentAdapter(photos.content)
                if (itemDecorationCount <= 0) {
                    addItemDecoration(TabContentItemsDecorator())
                }
            }
        }
    }
}