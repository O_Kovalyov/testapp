package com.gflexy.testapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gflexy.testapp.adapters.MainActivityViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        initTabLayout()
        subscribeToLiveData()
        getContent()
    }

    private fun getContent() {
        viewModel.getContent()
    }

    private fun subscribeToLiveData() {
        viewModel.publicContent.observe(this@MainActivity, Observer {
            vp_activity_main_content.adapter =
                MainActivityViewPagerAdapter(
                    supportFragmentManager,
                    viewModel.generateFragments(it),
                    resources.getStringArray(R.array.tabs)
                )
        })
    }

    private fun initTabLayout() {
        tl_activity_main_tabs.setupWithViewPager(vp_activity_main_content)
    }
}