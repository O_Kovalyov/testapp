package com.gflexy.testapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gflexy.testapp.model.*
import com.gflexy.testapp.service.Api.Companion.FORMAT
import com.gflexy.testapp.service.ApiService
import com.gflexy.testapp.service.InitialValues
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class MainActivityViewModel : ViewModel() {

    private val disposable: CompositeDisposable = CompositeDisposable()

    fun generateFragments(list: List<FragmentContent>): List<Fragment> {
        val listOfFragments: MutableList<Fragment> = ArrayList()
        for (items in InitialValues.values().indices) {
            val fragment: Fragment = InnerFragment()
            val bundle = Bundle()
            bundle.putString("content", Gson().toJson(list[items]))
            fragment.arguments = bundle
            listOfFragments.add(fragment)
        }
        return listOfFragments
    }

    private val content: MutableLiveData<List<FragmentContent>> = MutableLiveData()
    var publicContent: MutableLiveData<List<FragmentContent>> = content
        private set


    fun getContent() {
        disposable.add(Observable.zip(
            ApiService.getApi().getGroupID(App.API_KEY, InitialValues.FIRST.value, FORMAT, 1),
            ApiService.getApi().getGroupID(App.API_KEY, InitialValues.SECOND.value, FORMAT, 1),
            BiFunction { groupIDFirst: Response, groupIDSecond: Response ->
                Timber.e(groupIDFirst.toString())
                listOf(groupIDFirst.group, groupIDSecond.group)
            })
            .flatMap { list ->
                Observable.zip(
                    ApiService.getApi().getGroupPhotos(
                        App.API_KEY,
                        list[0].id,
                        null,
                        null,
                        null,
                        10,
                        1,
                        "json",
                        1
                    ),
                    ApiService.getApi().getGroupPhotos(
                        App.API_KEY,
                        list[1].id,
                        null,
                        null,
                        null,
                        10,
                        1,
                        "json",
                        1
                    ),
                    BiFunction { photosFirstTab: PhotosResult, photosSecondTab: PhotosResult ->
                        listOf(photosFirstTab.photos, photosSecondTab.photos)
                    }
                )
            }
            .flatMap { list ->
                Observable.just(generateListOfFragmentContent(list))
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnError { e -> Timber.e("$e") }
            .doOnNext { result -> content.value = result }
            .subscribe())
    }

    private fun generateListOfFragmentContent(list: List<Photos>): List<FragmentContent> {
        val fragmentContentList: MutableList<FragmentContent> = ArrayList()
        for (item in list.indices) {
            val content: MutableList<Content> = ArrayList()
            for (photo in list[item].photos.indices) {
                content.add(
                    Content(
                        photoLinkGenerator(list[item].photos[photo]),
                        list[item].photos[photo].title
                    )
                )
            }
            fragmentContentList.add(FragmentContent(content))
        }
        return fragmentContentList
    }

    private fun photoLinkGenerator(photo: Photo): String {
        return "https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg"
    }

    override fun onCleared() {
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
        super.onCleared()
    }
}