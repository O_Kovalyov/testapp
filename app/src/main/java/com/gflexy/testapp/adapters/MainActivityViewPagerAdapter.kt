package com.gflexy.testapp.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class MainActivityViewPagerAdapter(fragmentManager: FragmentManager,
                                   private val listOfFragments: List<Fragment>,
                                   private val listOfTitles: Array<String>) : FragmentPagerAdapter(fragmentManager){

    override fun getItem(position: Int): Fragment =
        listOfFragments[position]

    override fun getCount(): Int = listOfFragments.size

    override fun getPageTitle(position: Int): CharSequence? {
        return listOfTitles[position]
    }
}

