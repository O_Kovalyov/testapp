package com.gflexy.testapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.gflexy.testapp.R
import com.gflexy.testapp.model.Content
import kotlinx.android.synthetic.main.activity_main_view_pager_tab_item.view.*

class ViewPagerTabContentAdapter(private val photos: List<Content>) :
    RecyclerView.Adapter<ViewPagerTabContentAdapter.TabContentViewHolder>() {

    inner class TabContentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val photo: ImageView = view.iv_view_pager_tab_photo
        val title: TextView = view.tv_view_pager_tab_photo_name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabContentViewHolder =
        TabContentViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_main_view_pager_tab_item, parent, false)
        )

    override fun getItemCount(): Int = photos.size

    override fun onBindViewHolder(holder: TabContentViewHolder, position: Int) {
        holder.apply {
            Glide.with(photo.context)
                .load(photos[position].photoUrl)
                .apply(
                    RequestOptions().transform(
                        CenterCrop(),
                        RoundedCorners(10)
                    )
                )
                .into(photo)
            title.text = photos[position].photoTitle
        }
    }
}
