package com.gflexy.testapp.decoratos

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.gflexy.testapp.R
import kotlin.math.roundToInt

class TabContentItemsDecorator: RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        val position = parent.getChildAdapterPosition(view)
        if(position == 0){
            outRect.top = view.resources.getDimension(R.dimen.view_pager_tab_first_item_top_margin).roundToInt()
        }
        outRect.apply {
            bottom = view.resources.getDimension(R.dimen.view_pager_tab_bottom_margins).roundToInt()
            left = view.resources.getDimension(R.dimen.view_pager_tab_item_side_margins).roundToInt()
            right = view.resources.getDimension(R.dimen.view_pager_tab_item_side_margins).roundToInt()
        }
    }
}