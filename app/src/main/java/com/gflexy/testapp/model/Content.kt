package com.gflexy.testapp.model

data class Content (
    val photoUrl: String,
    val photoTitle: String
)