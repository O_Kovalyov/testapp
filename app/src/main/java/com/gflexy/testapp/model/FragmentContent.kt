package com.gflexy.testapp.model

data class FragmentContent (val content: List<Content>)