package com.gflexy.testapp.model

import com.google.gson.annotations.SerializedName

data class Group (@SerializedName("id") var id: String)