package com.gflexy.testapp.model

import com.google.gson.annotations.SerializedName

data class Photo (
    @SerializedName("id") val id: Long,
    @SerializedName("owner") val owner: String,
    @SerializedName("secret") val secret: String,
    @SerializedName("server") val server: String,
    @SerializedName("farm") val farm: String,
    @SerializedName("title") val title: String,
    @SerializedName("ispublic") val isPublic: Int,
    @SerializedName("isfriend") val isFriend: Int,
    @SerializedName("isfamily") val isFamily: Int,
    @SerializedName("ownername") val ownerName: String,
    @SerializedName("dateadded") val dateAdded: Long
)