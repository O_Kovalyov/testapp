package com.gflexy.testapp.model

import com.google.gson.annotations.SerializedName

data class PhotosResult (
    @SerializedName("photos")
    val photos: Photos
)