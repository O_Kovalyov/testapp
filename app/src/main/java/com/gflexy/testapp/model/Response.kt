package com.gflexy.testapp.model

import com.google.gson.annotations.SerializedName

data class Response (@SerializedName("group")
                    var group: Group)