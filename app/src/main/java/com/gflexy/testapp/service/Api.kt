package com.gflexy.testapp.service

import com.gflexy.testapp.model.PhotosResult
import com.gflexy.testapp.model.Response
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface Api {

    companion object{
        const val FORMAT = "json"
    }

    @POST("?method=flickr.urls.lookupGroup")
    @FormUrlEncoded
    fun getGroupID(
        @Field("api_key") key: String,
        @Field("url") url: String,
        @Field("format") format: String,
        @Field("nojsoncallback") nojsoncallback: Int
    ): Observable<Response>

    @POST("?method=flickr.groups.pools.getPhotos")
    @FormUrlEncoded
    fun getGroupPhotos(
        @Field("api_key") ket: String,
        @Field("group_id") groupId: String,
        @Field("tags") tags: String?,
        @Field("user_id") userID: String?,
        @Field("extras") extras: String?,
        @Field("per_page") perPage: Int,
        @Field("page") page: Int,
        @Field("format") format: String,
        @Field("nojsoncallback") nojsoncallback: Int
    ): Observable<PhotosResult>
}