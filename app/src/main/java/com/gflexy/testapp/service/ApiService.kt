package com.gflexy.testapp.service

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

object ApiService {

    private val interceptor: Interceptor =
        HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
            Timber.tag("OkHttp").d(message)
        }).setLevel(HttpLoggingInterceptor.Level.BODY)


    private val client: OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

    private val retrofit: Retrofit =
        Retrofit.Builder()
            .baseUrl("https://api.flickr.com/services/rest/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(client)
            .build()


    fun getApi(): Api {
        return retrofit.create(Api::class.java)
    }
}