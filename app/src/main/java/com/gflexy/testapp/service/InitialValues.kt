package com.gflexy.testapp.service

enum class InitialValues(val value: String) {
    FIRST("https://www.flickr.com/groups/catchy/"), SECOND("https://www.flickr.com/groups/flickrfriday/")
}